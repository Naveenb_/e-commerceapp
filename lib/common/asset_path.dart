

class AssetsPath {

  ///example to store assetes path
  /*static const String pathSobhaLogo = "assets/sobha_care_logo.png";*/

  ///login page
  static const String loginImage = "assets/login_image.png";
  static const String phoneImage = "assets/phone_icon.png";
  static const String googleImage = "assets/google_icon.png";
  static const String facebookImage = "assets/facebook_icon.png";

}