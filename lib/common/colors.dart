import 'package:flutter/material.dart';


const primaryColor = Color(0xFFfdfdfd);
const secondaryColor = Color(0xFFff8051);
const backgroundColor = Color(0xFFFFFFFF);
const textColorWhite = Color(0xFFFFFFFF);

const accentColor = Color(0xFF287194);
const secondarySplashColor = Color(0xFFff8051);

const primaryTextColor = Color(0xFF505050);
const primaryTextColorBlue = Color(0xFF287194);
const secondaryTextColor = Color(0xFF023246);



const whiteColor = Colors.white;
const errorColor = Colors.redAccent;
const successColor = Color(0xFF3bae69);

