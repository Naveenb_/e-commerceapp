
import 'package:flutter/material.dart';

class Constants {

  static const String appVersion = "2.0.0";
  static const String versionText = "version";

  ///APP VERSION, ITS IS REFERENCE FOR IS_UPDATE_AVAILABLE SERVICE
  static const String buildVersion = "1";
  static const String serviceUpdateVersion = "1.0";

  static const String appStoreUrl = "";
  static const String playStoreUrl = "";
  static const String privacyTermsUrl = "";
  static String appPlatform = "";

  ///Login Page
  static String emailIDTextInputHint = "Email ID";
  static String loginButtonText = "Send OTP";
  static String dividerLineText = " OR ";



  ///Base url example
  /*static const String baseUrl = 'https://sobhacare-api.sobhaapps.com/api/';*/




}