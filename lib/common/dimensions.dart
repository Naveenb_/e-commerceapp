

const defaultPadding = 16.0;
const defaultPaddingMin = 8.0;
const defaultBorderRadius = 10.0;
const defaultThemeCardRadius = 16.0;
const defaultElevation = 8.0;


const titleFontSize = 18.0;
const subTitleFontSize = 17.0;
const textFontSize = 14.0;
const textLargeFontSize = 16.0;
const buttonTextLargeFontSize = 20.0;

///login page
const defaultMarginMax = 20.0;
const textFormFieldHeight = 22.0;
const socialMediaLogo1Size = 14.0;
const socialMediaLogo2Size = 8.0;
const socialMediaLogo3Size = 0.0;


const zero = 0;