import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefUtils{

  static void storeIntData(String key, int data,) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt(key,data);
  }

  static void storeStringData(String key, String data,) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key,data);
  }

  static Future<int?> getIntData(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(key);
  }

  static Future<String?> getStringData(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }


}