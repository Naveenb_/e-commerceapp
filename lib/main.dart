import 'dart:io';

import 'package:flutter/material.dart';
import 'package:starcart/common/colors.dart';
import 'package:starcart/pages/login/login_page.dart';

import 'common/dimensions.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Star Cart',
      theme: _buildTheme(),
      initialRoute: '/',
      routes: {
        '/': (context) => /*SplashPage()*/const LoginPage(),

      },
    );
  }
}


//this class is for approve ssl certificate issue
class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext? context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}

ThemeData _buildTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    primaryColor: primaryColor,
    backgroundColor: backgroundColor,
    appBarTheme: _appBarTheme(base.appBarTheme),
    textTheme: _textTheme(base.textTheme),
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor: textColorWhite,
    ), colorScheme: ColorScheme.fromSwatch().copyWith(secondary: primaryTextColorBlue),
  );
}

AppBarTheme _appBarTheme(AppBarTheme base) =>
    base.copyWith(color: primaryColor,
        brightness: Brightness.light,
        textTheme: const TextTheme(headline6: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 18.0,
            fontFamily: 'Nunito',
            color: secondaryTextColor)),
        iconTheme: const IconThemeData(color: accentColor));

TextTheme _textTheme(TextTheme base) {
  return base.copyWith(
    headline1: base.headline1!.copyWith(
        fontWeight: FontWeight.w500,
        fontFamily: 'Nunito',
        color: primaryTextColor),

    headline6: base.headline6!.copyWith(
        fontFamily: 'Nunito',
        color: primaryTextColor),

    subtitle1: base.subtitle1!.copyWith(
      //fontSize: TITLE_FONT_SIZE,
      //    fontWeight: FontWeight.w600,
        fontFamily: 'Nunito',
        color: primaryTextColor),

    caption: base.caption!.copyWith(
        fontWeight: FontWeight.w400,
        fontFamily: 'Nunito',
        //  fontSize: TEXT_FONT_SIZE,
        color: primaryTextColor),

    bodyText1: base.bodyText1!.copyWith(
        fontWeight: FontWeight.w300,
        fontFamily: 'Nunito',
        //   fontSize: TEXT_FONT_SIZE,
        color: primaryTextColor),

    bodyText2: base.bodyText2!.copyWith(
        fontWeight: FontWeight.w400,
        fontFamily: 'Nunito',
        //  fontSize: TEXT_LARGE_FONT_SIZE,
        color: primaryTextColor),

    button: const TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.w500,
        fontFamily: 'Nunito',
        fontSize: buttonTextLargeFontSize),
  );


}