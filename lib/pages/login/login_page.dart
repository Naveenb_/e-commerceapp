import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:starcart/common/asset_path.dart';
import 'package:starcart/common/colors.dart';
import 'package:starcart/common/constants.dart';
import 'package:starcart/common/dimensions.dart';
import 'package:starcart/widgets/default_button.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var size;
  final _formKey = GlobalKey<FormState>();
  TextEditingController _emailIdTextController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }


  String? validateEmail(String? value) {
    RegExp regex = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    if (!regex.hasMatch(value!)) {
      return 'Enter a valid email address';
    } else {
      return null;
    }
  }

  void validateInputFields() {

    if(_formKey.currentState!.validate()){
      _formKey.currentState!.save();
      print("************************************************** Login Form Validated");

    }else{
      print("************************************************** Login Form Not Validated");
    }

  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: secondaryColor,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Stack(
          children: [
            Form(
              key: _formKey,
              child: ListView(
                children: [
                  loginImage(),
                  const SizedBox(height: defaultPadding),
                  inputFields(),
                  divider(),
                  const SizedBox(height: defaultPadding),
                  socialMedia(),
                  Text(
                    "By continuing, you agree to Brand’s \n Terms of Use and Privacy Policy",
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText1!
                        .copyWith(color: textColorWhite),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget loginImage() => Container(
        height: size.height * 0.4,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(AssetsPath.loginImage),
            fit: BoxFit.fill,
          ),
        ),
      );

  Widget inputFields() {
    Widget emailIdTextField() => TextFormField(
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(textFormFieldHeight),
            labelText: Constants.emailIDTextInputHint,
            labelStyle: Theme.of(context)
                .textTheme
                .bodyText1!
                .copyWith(color: textColorWhite),
            counterText: "",
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(defaultBorderRadius),
              borderSide: const BorderSide(
                color: whiteColor,
              ),
            ),

            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(defaultBorderRadius),
              borderSide: const BorderSide(color: whiteColor),
            ),

            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(defaultBorderRadius),
              borderSide: const BorderSide(color: whiteColor),
            ),

            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(defaultBorderRadius),
              borderSide: const BorderSide(color: errorColor),
            ),

            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(defaultBorderRadius),
              borderSide: const BorderSide(color: errorColor),
            ),
            //fillColor: Colors.green
          ),
          keyboardType: TextInputType.text,
          maxLines: 1,
          maxLength: 30,
          style: Theme.of(context)
              .textTheme
              .bodyText2!
              .copyWith(color: textColorWhite),
          controller: _emailIdTextController,
          validator: validateEmail,
        );

    Widget sendOtpBtn() => DefaultButton(
          buttonText: Constants.loginButtonText,
          size: size,
          onPressed: () => validateInputFields(),
        );

    return Padding(
      padding: const EdgeInsets.all(defaultPadding),
      child: Column(
        children: [
          emailIdTextField(),
          const SizedBox(height: defaultPadding),
          const SizedBox(height: defaultPadding),
          sendOtpBtn(),
        ],
      ),
    );
  }

  Widget divider() {
    return Padding(
      padding: const EdgeInsets.all(defaultPadding),
      child: Row(
        children: [
          const Expanded(
              child: Divider(
            color: whiteColor,
            thickness: 2,
          )),
          Text(
            Constants.dividerLineText,
            textAlign: TextAlign.center,
            style: Theme.of(context)
                .textTheme
                .bodyText1!
                .copyWith(color: textColorWhite),
          ),
          const Expanded(
              child: Divider(
            color: whiteColor,
            thickness: 2,
          )),
        ],
      ),
    );
  }

  Widget socialMedia() => SizedBox(
        height: size.height * 0.1,
        child: Row(
          children: [
            const SizedBox(width: defaultPadding),
            const SizedBox(width: defaultPadding),
            Expanded(
                child: Container(
              margin: const EdgeInsets.all(defaultMarginMax),
              padding: const EdgeInsets.all(socialMediaLogo1Size),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(defaultBorderRadius),
                color: whiteColor,
              ),
              child: Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(AssetsPath.phoneImage),
                  ),
                ),
              ),
            )),
            Expanded(
                child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(defaultBorderRadius),
                color: whiteColor,
              ),
              margin: const EdgeInsets.all(defaultMarginMax),
              padding: const EdgeInsets.all(socialMediaLogo2Size),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(defaultBorderRadius),
                  image: const DecorationImage(
                    image: AssetImage(AssetsPath.googleImage),
                  ),
                ),
              ),
            )),
            Expanded(
                child: Container(
              margin: const EdgeInsets.all(defaultMarginMax),
              padding: const EdgeInsets.all(socialMediaLogo3Size),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(defaultBorderRadius),
                color: whiteColor,
              ),
              child: Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(AssetsPath.facebookImage),
                  ),
                ),
              ),
            )),
            const SizedBox(width: defaultPadding),
            const SizedBox(width: defaultPadding),
          ],
        ),
      );
}
