
class BaseResponse {
  BaseResponse({
    required this.status,
    required this.message,
  });

  bool status;
  String message;

  factory BaseResponse.fromJson(Map<String, dynamic> json) => BaseResponse(
    status: json["status"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
  };
}