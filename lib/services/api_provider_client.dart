import 'dart:convert';
import 'package:http/http.dart';
import 'api_exception.dart';

class ApiProviderClient {




  Client client = Client();

  dynamic _handleResponse(Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:

      default:
        throw FetchDataException(
            'Error occurred while Communication with Server with StatusCode: ${response.statusCode}');
    }
  }

  ///example code to connect with the server.///
  /*/// CHECK LOGIN CREDENTIALS
  Future<LoginResponse> checkUserLoginDetails(String empId, String password) async {

    Map<String, dynamic> params = new Map();
    params["user_id"] = empId;
    params["password"] = password;

    print(params.toString());
    var url = Uri.parse(Constants.BASE_URL + checkUserLoginPath,);

    final response = await client.post(url,
        headers: {'Content-type': 'application/json'},
        body: json.encoder.convert(params));
    var responseJson = _handleResponse(response);
    print(response.body.toString());
    return LoginResponse.fromJson(responseJson);
  }

  /// UPDATE NEW PASSWORD
  Future<BaseResponse> changePassword(int technicianId , String password) async {
    Map<String, dynamic> params = new Map();
    params["id"] = technicianId;
    params["new_password"] = password;

    print(params.toString());
    var url = Uri.parse(Constants.BASE_URL + changePasswordPath,);

    final response = await client.put(url,
        headers: {'Content-type': 'application/json'},
        body: json.encoder.convert(params));
    var responseJson = _handleResponse(response);
    print(response.body.toString());
    return BaseResponse.fromJson(responseJson);
  }

  /// CHOOSE LANGUAGE
  Future<BaseResponse> chooseLanguage(int technicianId , String language) async {

    Map<String, dynamic> params = new Map();
    params["id"] = technicianId;
    params["language"] = language;

    print(params.toString());
    var url = Uri.parse(Constants.BASE_URL + chooseLanguagePath,);
    final response = await client.put(url,
        headers: {'Content-type': 'application/json'},
        body: json.encoder.convert(params));
    var responseJson = _handleResponse(response);
    print(response.body.toString());
    return BaseResponse.fromJson(responseJson);
  }

  /// GET ALL SC PROGRAMS
  Future<ScProgramsResponse> getAllScPrograms(int technicianId) async {
    var url = Uri.parse(Constants.BASE_URL + scProgramsPath + technicianId.toString(),);

    final response = await client.get(url,
        headers: {'Content-type': 'application/json'});
    var responseJson = _handleResponse(response);
    print(response.body.toString());
    return ScProgramsResponse.fromJson(responseJson);
  }

  /// MY PROFILE RESPONSE
  Future<ProfileResponse> getAllProfileTechnicianDetail(int technicianId) async {

    var url =Uri.parse(Constants.BASE_URL + myProfilePath + technicianId.toString(),);

    final response = await client.get(url,
        headers: {'Content-type': 'application/json'});
    var responseJson = _handleResponse(response);
    print(response.body.toString());
    return ProfileResponse.fromJson(responseJson);
  }*/


}




