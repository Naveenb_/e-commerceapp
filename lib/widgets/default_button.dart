import 'package:flutter/material.dart';
import 'package:starcart/common/colors.dart';
import 'package:starcart/common/dimensions.dart';

class DefaultButton extends StatelessWidget {

  const DefaultButton({Key? key, required this.buttonText, required this.onPressed, required this.size}) : super(key: key);

  final String buttonText;
  final VoidCallback onPressed;
  final Size size;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      minWidth: size.width,
      onPressed: () => onPressed(),
      color: whiteColor,
      splashColor: secondarySplashColor,
      padding: const EdgeInsets.all(16),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(defaultPaddingMin),
      ),
      child: Text(
        buttonText,
        style: Theme.of(context).textTheme.button!.copyWith(fontSize: 16, fontWeight: FontWeight.bold, color: primaryTextColorBlue),
        textAlign: TextAlign.center,
      ),
    );
  }
}
